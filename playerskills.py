from playercreation import Player
import playercreation
import MainLauncher

playercreation.createchar()


class PlayerSkills:
    def __init__(self, s_name, s_damage, s_acu, s_known):
        self.s_name = s_name
        self.s_damage = s_damage
        self.s_acu = s_acu
        self.s_known = s_known


skill_fireball = PlayerSkills("<1> Fireball", 3, 2, False)
skill_thunderblast = PlayerSkills("<2> Thunder blast", 4, 1, False)
skill_deathgrip = PlayerSkills("<3> Death grip", 4, 2, False)

skill_deepcut = PlayerSkills("<1: Deep cut", 3, 2, False)
skill_doublestrike = PlayerSkills("<2: Double strike", 4, 1, False)
skill_criplingcut = PlayerSkills("<3: Cripling cut", 3, 3, False)

wizard_skills = [skill_fireball, skill_thunderblast, skill_deathgrip]
warrior_skills = [skill_deepcut, skill_doublestrike, skill_criplingcut]


skills_known = int(MainLauncher.player_char.p_lvl)
# player_char.p_lvl = 1
# class_skills = wizard_skills
player_skills_k = []


def sks():
    global player_skills_k
    if MainLauncher.player_char.p_class == "warrior":
        player_skills_k = warrior_skills
    elif MainLauncher.player_char.p_class == "wizard":
        player_skills_k = wizard_skills
    else:
        print("yy")


sks()
print("skills known: " + str(player_skills_k[:skills_known]))
