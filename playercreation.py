from os import system, name


def clear():
    if name == 'nt':
        _ = system('cls')


class Player:
    def __init__(self, p_name, p_lvl, p_xphave, p_class):
        self.p_name = p_name
        self.p_lvl = p_lvl
        self.p_xphave = p_xphave
        self.p_class = p_class


class PlayerClass:
    def __init__(self, c_name, c_hp, c_ar, c_dmgmod):
        self.c_name = c_name
        self.c_hp = c_hp
        self.c_ar = c_ar
        self.c_dmgmod = c_dmgmod


class_warrior = PlayerClass("warrior", 10, 3, 3)
class_wizard = PlayerClass("wizard", 8, 3, 4)

player_name = "BLANK"
loop = True
playerclass = "pp"


def createchar():
    global playerclass
    while loop:
        print("<1> Warrior:\nUses melee and ranged simple, martial weapons. No magic power. Wear medium-heavy armor.")
        print("<2> Wizard:\nUses magical weapons, usually wands, staffs, crystals. Physically weak. Wear light armor.")
        player_class = input("Select your class\n>>")
        if int(player_class) == 1:
            playerclass = class_warrior.c_name
            break
        elif int(player_class) == 2:
            playerclass = class_wizard.c_name
            break
        else:
            clear()
            print("ERROR try again!")
    print(playerclass)

