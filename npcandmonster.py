class Monster:
    def __init__(self, m_name, m_lvl, m_hp, m_ar, m_maxdmg, m_xpgive):
        self.m_name = m_name
        self.m_lvl = m_lvl
        self.m_hp = m_hp
        self.m_ar = m_ar
        self.m_maxdmg = m_maxdmg
        self.m_xpgive = m_xpgive


monster_skeleton = Monster("skeleton", 1, 10, 3, 4, 4)
monster_zombie = Monster("zombie", 1, 9, 4, 3, 4)
monster_goblin = Monster("goblin", 1, 6, 3, 3, 2)

monster_list1 = [monster_goblin.m_name, monster_skeleton.m_name, monster_zombie.m_name]
